package com.ander.entity;

import org.bson.codecs.pojo.annotations.BsonId;
import org.springframework.data.mongodb.core.mapping.Document;

// @Document(collection = "transaction")  es Para MongoDB
@Document(collection = "transaction")
public class Transaction {

	
    // Crea coleción para  MongoDB Atlas 
	//( Mismos campos que la tabla transactioon ms-deposit, para que escuche los mismos atributos)
	
    @BsonId
    private String id ;
    private double amount ;
    private String type;
    private int accountId;
    
    // --- campo solicitado ----
    private String creationDate;  
        
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
    
	// -------------------------
	
	

	public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public int getAccountId() {
        return accountId;
    }
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }
}
