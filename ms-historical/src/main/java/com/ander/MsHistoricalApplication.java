package com.ander;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsHistoricalApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsHistoricalApplication.class, args);
	}

}
