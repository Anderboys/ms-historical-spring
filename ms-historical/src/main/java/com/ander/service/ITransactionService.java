package com.ander.service;

import com.ander.entity.Transaction;

public interface ITransactionService {

    public Transaction save (Transaction transaction);
    public Iterable<Transaction> findByAccountId (Integer accountId);
    public Iterable <Transaction> findAll();
}
