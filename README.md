# ms-historical-spring

- Ide: Spring Tool Suite 4 
- Version: 4.11.0.RELEASE
- java Buil Path 11.0.5
- pom.xml  <version>2.5.6</version>
- <java.version>11</java.version>
- Docker file
**Proyecto listo para dokerizar**

```
FROM openjdk:11.0.15-jdk
COPY "./target/ms-historical.jar" "ms-historical-app.jar"
EXPOSE 8082
ENTRYPOINT ["java","-jar","ms-historical-app.jar"]

- las configuraciones de este proyecto esta directamente para dockerizar
